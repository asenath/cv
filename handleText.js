import { text } from "./text.js"

window.onload = () => {
    document.querySelector(".artDesc").innerHTML = text.art
    document.querySelector(".musicDesc").innerHTML = text.music
    document.querySelector(".devDesc").innerHTML = text.dev
    console.log(text)
}