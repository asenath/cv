let description



window.onload = () => {
    description = document.querySelector(".description")
    handleFadingIn()
    if (document.documentElement.scrollTop >= window.innerHeight) {
        scroll(0.7, 0.3)
        scroll(1, 0)
    }
}

const handleScroll = () => {
    const height = window.innerHeight
    const position = document.documentElement.scrollTop
    const scale = position/height
    const reverse_scale = 1 - position/height
    scroll(scale, reverse_scale)
}

const scroll = (scale, reverse_scale) => {
    if (reverse_scale >= 0.3) {
        document.querySelector('#name h1').style.fontSize = reverse_scale * 7 + 'vh'
        document.querySelector('#name p').style.fontSize = reverse_scale * 4 + 'vh'
    }
    if (reverse_scale >= 0)  {
        document.querySelector('#name').style.marginTop = reverse_scale * 40 + 'vh'
        document.querySelector('body').style.backgroundColor = 'rgba(22, 22, 22,'+ scale +')'
    }
    if (reverse_scale < 0) {
        document.querySelector('#name h1').style.fontSize = 0.3 * 7 + 'vh'
        document.querySelector('#name p').style.fontSize = 0.3 * 4 + 'vh'
        document.querySelector('#name').style.marginTop = '.2em'
        document.querySelector('body').style.backgroundColor = 'rgba(22, 22, 22, 1)'
    }
}


const handleFadingIn = () => {
    const faders = document.querySelectorAll('.fade-in')

    const appearOptions = {
        threshold: 1,
        rootMargin: '0px 0px 10% 0px'
    }

    const appearOnScroll = new IntersectionObserver(function (entries, appearOnScroll) {
        entries.forEach((entry) => {
            if (!entry.isIntersecting) {
                return 0
            } else {
                if (entry.target.classList.contains('back-title')) {
                    document.querySelector(".contact-wrapper").classList.add("animateAbout")
                }
                entry.target.classList.add('appear')
                appearOnScroll.unobserve(entry.target)
            }
        })
    }, appearOptions)

    faders.forEach((fader) => {
        appearOnScroll.observe(fader)
    })
}



const showContact = () => {
    document.querySelector(".contact-popup").classList.add("opened")
    document.querySelectorAll(".arrow").forEach(arrow => arrow.classList.add("arrow-open"))
    document.querySelector(".contact-button").setAttribute( "onClick", "closeContact()")
}

const closeContact = () => {
    document.querySelector(".contact-popup").classList.remove("opened")
    document.querySelectorAll(".arrow").forEach(arrow => arrow.classList.remove("arrow-open"))
    document.querySelector(".contact-button").setAttribute( "onClick", "showContact()")
}

const showNew = (e) => {
    e.children[0].style.opacity = "1"
    e.children[1].style.opacity = "0"
}

const showInitial = (e) => {
    e.children[0].style.opacity = "0"
    e.children[1].style.opacity = "1"
}

const slide = () => {
    document.querySelector(".music-description").style.transform = "translateX(-100%)"
    document.querySelector(".art-description").style.transform = "translateX(0)"
    document.querySelector(".dots span:nth-child(1)").style.color = "#383838"
    document.querySelector(".dots span:nth-child(2)").style.color = "#bdbdbd"
}

const slideBack = () => {
    document.querySelector(".music-description").style.transform = "translateX(0)"
    document.querySelector(".art-description").style.transform = "translateX(100%)"
    document.querySelector(".dots span:nth-child(2)").style.color = "#383838"
    document.querySelector(".dots span:nth-child(1)").style.color = "#bdbdbd"
}